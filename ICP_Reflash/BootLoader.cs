﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.IO.Ports;

namespace ICP_Reflash
{
    // Command options for boot loader
    public enum BootLoaderCommand {
        NONE = 0,
        READ_BOOT_INFO,
	    ERASE_FLASH, 
	    PROGRAM_FLASH,
	    READ_CRC,
	    JMP_TO_APP
    };

    // Transmit state
    public enum TransmitState
    {
        TX_STATE_IDLE,
        TX_STATE_FIRST_SEND,
        TX_STATE_RETRY
    };

    // Handler for bootloader communications
    public class BootLoader
    {
        // Constants
        public const int MAX_TX_BUFFER = 1000;
        public const int MAX_RX_BUFFER = 255;

        // Special characters
        public const int SOH = 0x01;
        public const int EOT = 0x04;
        public const int DLE = 0x10;

        // Hex file manager
        private HexFileManager _hexFileManager;

        // Packet transmission
        private int _maxRetry;
        private int _retryCount;
        private int _txRetryDelay;
        private int _txPacketLen;
        private byte[] _tx_packet;
        private TransmitState _transmitState;
        private DateTime _nextRetryTime;
        private BootLoaderCommand _lastSentCommand;

        // Packet reception
        private int _rxPacketLen;
        private bool _escape;
        private byte[] _rx_packet;
        private bool _rxFrameValid;

        // Flag for flash file programming
        private bool _resetFilePointer;

        // Serial port
        private SerialPort _serial_port;
        private bool _bThreadStopRequested;
        private Thread _serialReadThread;
        private byte[] _serial_buff;

        // Callback handler
        private IResponseProcessor _responseProcessor;

        // Constructor
        public BootLoader()
        {
            // Packet transmission
            _maxRetry = 0;
            _retryCount = 0;
            _txRetryDelay = 0;
            _txPacketLen = 0;
            _tx_packet = new byte[MAX_TX_BUFFER];
            _transmitState = TransmitState.TX_STATE_IDLE;
            _nextRetryTime = DateTime.Now;
            _lastSentCommand = BootLoaderCommand.NONE;

            // Packet reception
            _rx_packet = new byte[MAX_RX_BUFFER];
            _rxFrameValid = false;
            _rxPacketLen = 0;
            _escape = false;

            // Hex file mananger
            _hexFileManager = new HexFileManager();

            // Flag for flash file programming
            _resetFilePointer = true;

            // Serial port
            _serial_port = null;
            _serialReadThread = null;
            _bThreadStopRequested = false;

            // Callback handler
            _responseProcessor = null;
        }

        // Send a command to the remote unit
        public bool SendCommand(BootLoaderCommand cmd, int Retries, int DelayInMs)
        {
            byte[] buff = new byte[MAX_TX_BUFFER];
            ushort buflen = 0;
            ushort crc;

            switch (cmd)
            {
                case BootLoaderCommand.READ_BOOT_INFO:
		            buff[buflen++] = (byte) cmd;
		            _maxRetry = Retries;
                    _retryCount = Retries;	
		            _txRetryDelay = DelayInMs;
                    break;
                case BootLoaderCommand.ERASE_FLASH:
		            buff[buflen++] = (byte) cmd;
		            _maxRetry = Retries;
                    _retryCount = Retries;	
		            _txRetryDelay = DelayInMs;
                    break;
                case BootLoaderCommand.JMP_TO_APP:
		            buff[buflen++] = (byte) cmd;
		            _maxRetry = 1;
                    _retryCount = 1;	
		            _txRetryDelay = 10;
                    break;
                case BootLoaderCommand.PROGRAM_FLASH:
		            buff[buflen++] = (byte) cmd;
		            if(_resetFilePointer)
		            {
			            if(!_hexFileManager.ResetHexFilePointer())
			            {
				            // Error in resetting the file pointer
				            return false;
			            }
		            }
                    // Send 10 records per transaction
                    for (int nRecords = 0; nRecords < 10; nRecords++)
                    {
                        int HexRecLen = _hexFileManager.GetNextHexRecord();
                        if (nRecords == 0 && HexRecLen == 0)
                        {
                            // No more data to send
                            return false;
                        }
                        for (int i = 0; i < _hexFileManager.HexLine.Length; i++)
                        {
                            if (buflen < MAX_TX_BUFFER - 2)
                            {
                                buff[buflen++] = (byte)_hexFileManager.HexLine[i];
                            }
                            else
                            {
                                // Buffer overflow
                                return false;
                            }
                        }
                    }
		            _maxRetry = Retries;
                    _retryCount = Retries;	
		            _txRetryDelay = DelayInMs;
                    break;
                case BootLoaderCommand.READ_CRC:
		            buff[buflen++] = (byte) cmd;
		            _hexFileManager.VerifyFlash();
		            buff[buflen++] = (byte) (_hexFileManager.ProgramStartAddress);
		            buff[buflen++] = (byte) (_hexFileManager.ProgramStartAddress >> 8);
		            buff[buflen++] = (byte) (_hexFileManager.ProgramStartAddress >> 16);
		            buff[buflen++] = (byte) (_hexFileManager.ProgramStartAddress >> 24);
		            buff[buflen++] = (byte) (_hexFileManager.ProgramLength);
                    buff[buflen++] = (byte) (_hexFileManager.ProgramLength >> 8);
                    buff[buflen++] = (byte) (_hexFileManager.ProgramLength >> 16);
                    buff[buflen++] = (byte) (_hexFileManager.ProgramLength >> 24);
		            buff[buflen++] = (byte) (_hexFileManager.ProgramCrc);
                    buff[buflen++] = (byte) (_hexFileManager.ProgramCrc >> 8);
		            _maxRetry = Retries;
                    _retryCount = Retries;	
		            _txRetryDelay = DelayInMs;
                    break;
                default:
                    return false;
            }

            // Calculate CRC for the frame.
            crc = Utility.CalculateCrc(buff, 0, buflen);
            buff[buflen++] = (byte)crc;
            buff[buflen++] = (byte)(crc >> 8);

            // SOH: Start of header
            _txPacketLen = 0;
            _tx_packet[_txPacketLen++] = SOH;

            // Form TxPacket. Insert DLE in the data field whereever SOH and EOT are present.
            for (int i = 0; i < buflen; i++)
            {
                if (_txPacketLen < MAX_TX_BUFFER - 3)
                {
                    if ((buff[i] == EOT) || (buff[i] == SOH) || (buff[i] == DLE))
                    {
                        _tx_packet[_txPacketLen++] = DLE;
                    }
                    _tx_packet[_txPacketLen++] = buff[i];
                }
                else
                {
                    // Buffer overflow
                    return false;
                }
            }

            // EOT: End of transmission
            _tx_packet[_txPacketLen++] = EOT;
            _lastSentCommand = cmd;
            _transmitState = TransmitState.TX_STATE_FIRST_SEND;
            return true;
        }

        // Open a serial port for communications
        public void OpenPort(string comport, IResponseProcessor responseProcessor)
        {
            _responseProcessor = responseProcessor;
            _serial_port = new SerialPort(comport, 115200);
            _serial_port.Open();
            // Start a new thread to process the serial port
            ThreadStart myThreadDelegate = new ThreadStart(processSerialData);
            _serialReadThread = new Thread(myThreadDelegate);
            _serialReadThread.IsBackground = true;
            _serialReadThread.Start();
        }

        // Loop continually reading and writing serial data
        private void processSerialData()
        {
            _bThreadStopRequested = false;
            while (!_bThreadStopRequested)
            {
                if (_transmitState != TransmitState.TX_STATE_IDLE)
                {
                    handleTransmit();
                }
                if (_serial_port.BytesToRead > 0)
                {
                    handleReceive();
                }
                // Sleep if there is no activity 
                if (_transmitState == TransmitState.TX_STATE_IDLE)
                {
                    Thread.Sleep(50);
                }
            }
        }

        // Send or resend queued packets
        private void handleTransmit()
        {
            bool bNoResponseFromDevice;

            bNoResponseFromDevice = false;
            switch (_transmitState)
            {
                case TransmitState.TX_STATE_FIRST_SEND:
                    if (_retryCount > 0)
                    {
                        // *** DEBUG
                        Console.Write("Put: ");
                        for (int i = 0; i < _txPacketLen; i++)
                        {
                            Console.Write("{0:X2} ", _tx_packet[i]);
                        }
                        Console.WriteLine("\r\n");

                        // There is something to send.
                        _serial_port.Write(_tx_packet, 0, _txPacketLen);
                        _retryCount--;
                        // If there is no response to "first try", the command will be retried.
                        _transmitState = TransmitState.TX_STATE_RETRY;
                        // Next retry should be attempted only after a delay.
                        _nextRetryTime = DateTime.Now.AddMilliseconds(_txRetryDelay);
                    }
                    break;
                case TransmitState.TX_STATE_RETRY:
                    if (_retryCount > 0)
                    {
                        if (DateTime.Now.Subtract(_nextRetryTime).TotalMilliseconds > 0)
                        {
                            // Delay elapsed. Its time to retry.
                            _nextRetryTime = DateTime.Now.AddMilliseconds(_txRetryDelay);

                            // *** DEBUG
                            Console.Write("Retry Put: ");
                            for (int i = 0; i < _txPacketLen; i++)
                            {
                                Console.Write("{0:X2} ", _tx_packet[i]);
                            }
                            Console.WriteLine("\r\n");

                            _serial_port.Write(_tx_packet, 0, _txPacketLen);
                            // Decrement retry count.
                            _retryCount--;
                        }
                    }
                    else
                    {
                        // Retries Exceeded
                        bNoResponseFromDevice = true;
                    }
                    break;
            }

            // Handle no response situation depending on the last sent command.
            if (bNoResponseFromDevice)
            {
                bNoResponseFromDevice = false;
                switch (_lastSentCommand)
                {
                    case BootLoaderCommand.READ_BOOT_INFO:
                    case BootLoaderCommand.ERASE_FLASH:
                    case BootLoaderCommand.PROGRAM_FLASH:
                    case BootLoaderCommand.READ_CRC:
                        // Notify main window that there was no reponse.
                        _responseProcessor.process_timeout(_lastSentCommand);
                        break;
                }
                // Reset the state
                _transmitState = TransmitState.TX_STATE_IDLE;
                _lastSentCommand = BootLoaderCommand.NONE;
            }
        }

        // Stop retries
        public void StopTxRetries()
        {
            // Reset state.
            _transmitState = TransmitState.TX_STATE_IDLE;
        }

        // Process serial port receive
        private void handleReceive()
        {
            int buffLen;

            buffLen = _serial_port.BytesToRead;
            _serial_buff = new byte[buffLen];
            _serial_port.Read(_serial_buff, 0, buffLen);

            // *** DEBUG
            Console.Write("Got: ");
            for (int i = 0; i < buffLen; i++)
            {
                Console.Write("{0:X2} ", _serial_buff[i]);
            }
            Console.WriteLine("\r\n");

                BuildRxFrame(_serial_buff, buffLen);
	        if(_rxFrameValid)
	        {
		        // Valid frame is received.
		        // Disable further retries.
		        StopTxRetries();
		        _rxFrameValid = false;
		        // Handle Response
		        HandleResponse();
	        }
        }

        // Build up RX frames
        private void BuildRxFrame(byte[] buff, int buffLen)
        {
            int bufIndex = 0;
            ushort crc;

            while ((buffLen > 0) && (_rxFrameValid == false) && (_rxPacketLen < MAX_RX_BUFFER))
            {
                buffLen--;
                switch (buff[bufIndex])
                {
                    case SOH: //Start of header
                        if (_escape)
                        {
                            // Received byte is not SOH, but data.
                            _rx_packet[_rxPacketLen] = buff[bufIndex];
                            _rxPacketLen++;
                            // Reset _escape Flag.
                            _escape = false;
                        }
                        else
                        {
                            // Received byte is indeed a SOH which indicates start of new frame.
                            _rxPacketLen = 0;
                            _escape = false;
                        }
                        break;
                    case EOT: // End of transmission
                        if (_escape)
                        {
                            // Received byte is not EOT, but data.
                            _rx_packet[_rxPacketLen] = buff[bufIndex];
                            _rxPacketLen++;
                            // Reset _escape Flag.
                            _escape = false;
                        }
                        else
                        {
                            // Received byte is indeed a EOT which indicates end of frame.
                            // Calculate CRC to check the validity of the frame.
                            if (_rxPacketLen > 1)
                            {
                                crc = (ushort)((_rx_packet[_rxPacketLen - 2]) & 0x00ff);
                                crc |= (ushort)(((_rx_packet[_rxPacketLen - 1] << 8) & 0xFF00));
                                if ((Utility.CalculateCrc(_rx_packet, 0, (uint)(_rxPacketLen - 2)) == crc) && (_rxPacketLen > 2))
                                {
                                    // CRC matches and frame received is valid.
                                    _rxFrameValid = true;
                                }
                            }
                        }
                        break;
                    case DLE: // _escape character received.
                        if (_escape)
                        {
                            // Received byte is not ESC but data.
                            _rx_packet[_rxPacketLen] = buff[bufIndex];
                            _rxPacketLen++;
                            // Reset _escape Flag.
                            _escape = false;
                        }
                        else
                        {
                            // Received byte is an escape character. Set _escape flag to escape next byte.
                            _escape = true;
                        }
                        break;
                    default: // Data field.
                        _rx_packet[_rxPacketLen] = buff[bufIndex];
                        _rxPacketLen++;
                        // Reset _escape Flag.
                        _escape = false;
                        break;
                }
                // Increment the pointer.
                bufIndex++;
            }
        }

        // Handle response from device
        void HandleResponse()
        {
	        BootLoaderCommand cmd = (BootLoaderCommand) _rx_packet[0];

            switch (cmd)
	        {
	            case BootLoaderCommand.READ_BOOT_INFO:
	            case BootLoaderCommand.ERASE_FLASH:
	            case BootLoaderCommand.READ_CRC:
		            // Notify main window that command received successfully.
		            _responseProcessor.process_response(cmd, _rx_packet);
                    _lastSentCommand = BootLoaderCommand.NONE;
		            break;
	            case BootLoaderCommand.PROGRAM_FLASH:		
		            // If there is a hex record, send next hex record.
		            _resetFilePointer = false; // No need to reset hex file pointer.
		            if(!SendCommand(BootLoaderCommand.PROGRAM_FLASH, _maxRetry, _txRetryDelay))
		            {
			            // Notify main window that programming operation completed.
			            _responseProcessor.process_response(cmd, _rx_packet);
                        _lastSentCommand = BootLoaderCommand.NONE;
		            }
		            _resetFilePointer = true;
		            break;
	        }
        }

        // Check serial port status
        public bool GetPortOpenStatus()
        {
            return (_serial_port != null);
        }

        // Close the serial port
        public void ClosePort()
        {
            if (_serial_port != null)
            {
                _bThreadStopRequested = true;
                _serialReadThread.Join();
                try
                {
                    _serial_port.Close();
                }
                catch
                {
                    ;
                }
                _serial_port = null;
            }
        }

        // Provide feedback to the user about operation progress
        public double getProgress()
        {
            int numerator, denominator;

            numerator = 0;
            denominator = 0;
            switch (_lastSentCommand)
            {
                case BootLoaderCommand.READ_BOOT_INFO:
                case BootLoaderCommand.ERASE_FLASH:
                case BootLoaderCommand.READ_CRC:
                case BootLoaderCommand.JMP_TO_APP:
                    // Progress with respect to retry count.
                    numerator = (_maxRetry - _retryCount);
                    denominator = _maxRetry;
                    break;
                case BootLoaderCommand.PROGRAM_FLASH:
                    // Progress with respect to line counts in hex file.
                    numerator = _hexFileManager.HexCurrLineNo;
                    denominator = _hexFileManager.HexTotalLines;
                    break;
            }
            if (denominator == 0)
            {
                return 0.0;
            }
            return (100.0 * numerator / denominator);
        }

        // Report the hex file CRC
        public ushort CalculateFlashCRC()
        {
            _hexFileManager.VerifyFlash();
            return _hexFileManager.ProgramCrc;
        }

        // Load a hex file from disk
        public bool LoadHexFile()
        {
            return _hexFileManager.LoadHexFile();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICP_Reflash
{
    //
    // CCITT CRC-16 Utility routines for boot loader
    //
    public static class Utility
    {
        //
        // CCITT CRC-16 lookup table - based on X^16 + X^12 + X^5 + 1 polynomial
        //
        public static ushort[] crc_table = 
        {
            0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
            0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef
        };

        // CRC calculation - see app note for details
        public static ushort CalculateCrc(byte[] data, uint offset, uint len)
        {
            uint i, index;
            ushort crc = 0;

            index = 0;
            while ((len--) != 0)
            {
                i = (uint)((crc >> 12) ^ (data[offset + index] >> 4));
                crc = (ushort)(crc_table[i & 0x0F] ^ (crc << 4));
                i = (uint)((crc >> 12) ^ (data[offset + index] >> 0));
                crc = (ushort)(crc_table[i & 0x0F] ^ (crc << 4));
                index++;
            }

            return (ushort)(crc & 0xFFFF);
        }
    }
}

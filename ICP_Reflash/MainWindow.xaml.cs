﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO.Ports;
using System.Windows.Threading;

namespace ICP_Reflash
{
    // Port of PIC source code for bootloader from application note: AN1388 2014_02_14 
    public partial class MainWindow : Window, IResponseProcessor
    {
        // Constants
        public const int PROGRAM_TIMEOUT_MS = 5000;
        public const int ERASE_TIMEOUT_MS = 5000;
        public const int VERIFY_TIMEOUT_MS = 5000;
        public const int BOOT_INFO_TIMEOUT_MS = 200;
        public const int RUN_TIMEOUT_MS = 10;

        // Delegates
        public delegate void processResponseDelegate(BootLoaderCommand cmd, byte[] response);
        public delegate void processTimeoutDelegate(BootLoaderCommand cmd);

        // Member variables
        private BootLoader _bootLoader;
        private bool _bEraseProgramVerify;
        private bool _bConnected;
        private ButtonStates _buttonStates;

        // Timer for progress bar
        private DispatcherTimer _progressBarTimer;

        // Constructor
        public MainWindow()
        {
            InitializeComponent();

            // Initialize port list
            init_port_list();

            // Initialize member variables
            _bootLoader = new BootLoader();
            _bEraseProgramVerify = false;
            _bConnected = false;
            _buttonStates = ButtonStates.None;

            // Timer for progress bar updates at 100 milli-second interval
            _progressBarTimer = new DispatcherTimer();
            _progressBarTimer.Tick += _progressBarTimer_Tick;
            _progressBarTimer.Interval = new TimeSpan(0, 0, 0, 0, 333);
            _progressBarTimer.Start();
        }

        // Update the progress bar
        void _progressBarTimer_Tick(object sender, EventArgs e)
        {
            pbProgress.Value = _bootLoader.getProgress();
        }

        // Update the list of serial ports
        private void init_port_list()
        {
            if (SerialPort.GetPortNames().Length > 0)
            {
                foreach (string s in SerialPort.GetPortNames())
                {
                    cbPorts.Items.Add(s);
                }
                cbPorts.SelectedIndex = 0;
            }
            else
            {
                btnConnect.IsEnabled = false;
            }
        }

        // User requests connect operation
        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (_bConnected)
            {
                // Already connected. Disconnect now.
                _bConnected = false;
                if (_bootLoader.GetPortOpenStatus())
                {
                    // com port already opened. close com port
                    _bootLoader.ClosePort();
                }
                // Print console message.
                PrintToConsole("Device disconnected");
                // Update button state
                EnableAllButtons(false);
                btnConnect.Content = "Connect";
                btnConnect.IsEnabled = true;
            }
            else
            {
                // Close any existing connections
                if (_bootLoader.GetPortOpenStatus())
                {
                    // com port already opened. close com port
                    _bootLoader.ClosePort();
                }
                // Open Communication port freshly.
                string comport = (string)cbPorts.SelectedItem;
                _bootLoader.OpenPort(comport, (IResponseProcessor)this);
                if (_bootLoader.GetPortOpenStatus())
                {
                    // Trigger Read boot info command
                    _bootLoader.SendCommand(BootLoaderCommand.READ_BOOT_INFO, 30, BOOT_INFO_TIMEOUT_MS);
                    // Print a message to user/
                    PrintToConsole("Please reset device and invoke bootloader");
                    // Disable all buttons to avoid further operations
                    EnableAllButtons(false);
                }
            }
        }

        // User requests hex file load operation
        private void btnLoadHex_Click(object sender, RoutedEventArgs e)
        {
            if (_bootLoader.LoadHexFile())
            {
                PrintToConsole("Hex file loaded successfully");
                // Enable Program button
                btnProgram.IsEnabled = true;
                btnEraseThenProgram.IsEnabled = true;
            }
            else
            {
                PrintToConsole("Hex file loading failed");
            }	
        }

        // User requests hex file program operation
        private void btnProgram_Click(object sender, RoutedEventArgs e)
        {
            // Disable all buttons to avoid further operations
            SaveButtonStates();
            EnableAllButtons(false);
            _bootLoader.SendCommand(BootLoaderCommand.PROGRAM_FLASH, 3, PROGRAM_TIMEOUT_MS);
        }

        // User requests hex file verify operation
        private void btnVerify_Click(object sender, RoutedEventArgs e)
        {
            // Disable all buttons to avoid further operations
            SaveButtonStates();
            EnableAllButtons(false);
            _bootLoader.SendCommand(BootLoaderCommand.READ_CRC, 3, VERIFY_TIMEOUT_MS);
        }

        // User requests combined erase then program operation
        private void btnEraseThenProgram_Click(object sender, RoutedEventArgs e)
        {
            // Disable all buttons to avoid further operations
            SaveButtonStates();
            EnableAllButtons(false);
            _bEraseProgramVerify = true;
            // Start with erase. Rest is automatically handled by state machine.
            _bootLoader.SendCommand(BootLoaderCommand.ERASE_FLASH, 3, ERASE_TIMEOUT_MS);
        }

        // User requests application launch
        private void btnRunApplication_Click(object sender, RoutedEventArgs e)
        {
            _bootLoader.SendCommand(BootLoaderCommand.JMP_TO_APP, 1, RUN_TIMEOUT_MS);
            PrintToConsole("\r\nCommand issued to run application");
        }

        // User requests flash erase operation
        private void btnErase_Click(object sender, RoutedEventArgs e)
        {
            // Disable all buttons to avoid further operations
            SaveButtonStates();
            EnableAllButtons(false);
            _bootLoader.SendCommand(BootLoaderCommand.ERASE_FLASH, 3, ERASE_TIMEOUT_MS);
        }

        // User requests boot version
        private void btnBootVersion_Click(object sender, RoutedEventArgs e)
        {
            // Disable all buttons to avoid further operations
            SaveButtonStates();
            EnableAllButtons(false);
            _bootLoader.SendCommand(BootLoaderCommand.READ_BOOT_INFO, 50, BOOT_INFO_TIMEOUT_MS);
        }

        // Enable or disable all buttons
        private void EnableAllButtons(bool bEnable)
        {
            btnLoadHex.IsEnabled = bEnable;
            btnErase.IsEnabled = bEnable;
            btnProgram.IsEnabled = bEnable;
            btnVerify.IsEnabled = bEnable;
            btnEraseThenProgram.IsEnabled = bEnable;
            btnRunApplication.IsEnabled = bEnable;
            btnBootVersion.IsEnabled = bEnable;
        }

        // Print a message on the console
        private void PrintToConsole(string s)
        {
            tbConsole.Text += s;
            tbConsole.Text += "\r\n";
        }

        // Clear the console
        private void ClearConsole()
        {
            tbConsole.Text = string.Empty;
        }

        // Process response from boot loader
        public void process_response(BootLoaderCommand cmd, byte[] response)
        {
            int MajorVer;
            int MinorVer;
            string msg;
            ushort crc;

            // Make sure GUI updates are on the GUI thread
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(new processResponseDelegate(process_response), new object[] {cmd, response} );
                return;
            }

            MajorVer = response[1];
            MinorVer = response[2];

            switch (cmd)
            {
                case BootLoaderCommand.READ_BOOT_INFO:
                    if (_bConnected == false)
                    {
                        // New connection.
                        ClearConsole();
                        PrintToConsole("Device connected");
                    }
                    msg = string.Format("Bootloader Firmware Version: {0}.{1}", MajorVer, MinorVer);
                    PrintToConsole(msg);
                    RestoreButtonStates();
                    // Enable only load hex, Disconnect and erase buttons for next operation.
                    btnLoadHex.IsEnabled = true;
                    btnErase.IsEnabled = true;
                    // Change the connect button to disconnect.
                    _bConnected = true;
                    btnConnect.Content = "Disconnect";
                    // Disable com port combo box.
                    cbPorts.IsEnabled = false;
                    // Also enable bootloader version info.
                    btnBootVersion.IsEnabled = true;
                    break;

                case BootLoaderCommand.ERASE_FLASH:
                    PrintToConsole("Flash Erased");
                    if (_bEraseProgramVerify)// Operation Erase->Program->Verify
                    {
                        // Erase completed. Next operation is programming.
                        _bootLoader.SendCommand(BootLoaderCommand.PROGRAM_FLASH, 3, PROGRAM_TIMEOUT_MS);
                    }
                    // Restore button status to allow further operations.
                    RestoreButtonStates();
                    break;

                case BootLoaderCommand.PROGRAM_FLASH:
                    PrintToConsole("Programming completed");
                    // Restore button status to allow further operations.
                    RestoreButtonStates();
                    btnVerify.IsEnabled = true;
                    btnRunApplication.IsEnabled = true;
                    if (_bEraseProgramVerify)// Operation Erase->Program->Verify
                    {
                        // Programming completed. Next operation is verification.
                        _bootLoader.SendCommand(BootLoaderCommand.READ_CRC, 3, VERIFY_TIMEOUT_MS);
                    }
                    break;

                case BootLoaderCommand.READ_CRC:
                    crc = (ushort)(((response[2] << 8) & 0xFF00) | (response[1] & 0x00FF));
                    if (crc == _bootLoader.CalculateFlashCRC())
                    {
                        PrintToConsole("Verification successful");
                    }
                    else
                    {
                        PrintToConsole("Verification failed");
                    }
                    // Reset erase->program-verify operation.
                    _bEraseProgramVerify = false;
                    // Restore button status to allow further operations.
                    RestoreButtonStates();
                    btnVerify.IsEnabled = true;
                    btnRunApplication.IsEnabled = true;
                    break;
            }

            if (!_bConnected)
            {
                // Disable all buttons, if disconnected.
                EnableAllButtons(false);
            }
        }

        // Enable buttons that were previously enabled
        private void RestoreButtonStates()
        {
            btnLoadHex.IsEnabled = ((_buttonStates & ButtonStates.BtnLoadHex) != ButtonStates.None);
            btnErase.IsEnabled = ((_buttonStates & ButtonStates.BtnErase) != ButtonStates.None);
            btnProgram.IsEnabled = ((_buttonStates & ButtonStates.BtnProgram) != ButtonStates.None);
            btnVerify.IsEnabled = ((_buttonStates & ButtonStates.BtnVerify) != ButtonStates.None);
            btnEraseThenProgram.IsEnabled = ((_buttonStates & ButtonStates.BtnEraseProgramVerify) != ButtonStates.None);
            btnRunApplication.IsEnabled = ((_buttonStates & ButtonStates.BtnRunApplication) != ButtonStates.None);
            btnBootVersion.IsEnabled = ((_buttonStates & ButtonStates.BtnBootVersion) != ButtonStates.None);
        }

        // Save button state
        private void SaveButtonStates()
        {
            _buttonStates = ButtonStates.None;
            _buttonStates |= (btnLoadHex.IsEnabled ? ButtonStates.BtnLoadHex : ButtonStates.None);
            _buttonStates |= (btnErase.IsEnabled ? ButtonStates.BtnErase : ButtonStates.None);
            _buttonStates |= (btnProgram.IsEnabled ? ButtonStates.BtnProgram : ButtonStates.None);
            _buttonStates |= (btnVerify.IsEnabled ? ButtonStates.BtnVerify : ButtonStates.None);
            _buttonStates |= (btnEraseThenProgram.IsEnabled ? ButtonStates.BtnEraseProgramVerify : ButtonStates.None);
            _buttonStates |= (btnRunApplication.IsEnabled ? ButtonStates.BtnRunApplication : ButtonStates.None);
            _buttonStates |= (btnBootVersion.IsEnabled ? ButtonStates.BtnBootVersion : ButtonStates.None);
        }

        // Process timeout from boot loader
        public void process_timeout(BootLoaderCommand cmd)
        {
            // Make sure GUI updates are on the GUI thread
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(new processTimeoutDelegate(process_timeout), new object[] { cmd });
                return;
            }

            _bEraseProgramVerify = false;
            switch (cmd)
            {
                case BootLoaderCommand.READ_BOOT_INFO:
                case BootLoaderCommand.ERASE_FLASH:
                case BootLoaderCommand.PROGRAM_FLASH:
                case BootLoaderCommand.READ_CRC:
                    PrintToConsole("No Response from the device. Operation failed");
                    RestoreButtonStates();
                    break;
            }
        }
    }
}

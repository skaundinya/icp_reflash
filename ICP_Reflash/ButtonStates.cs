﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICP_Reflash
{
    [Flags]
    public enum ButtonStates
    {
        None = 0,
        BtnLoadHex = 1,
        BtnErase = 2,
        BtnProgram = 4,
        BtnVerify = 8,
        BtnEraseProgramVerify = 16,
        BtnRunApplication = 32,
        BtnBootVersion = 64
    }
}

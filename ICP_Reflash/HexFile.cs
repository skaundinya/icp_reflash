﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace ICP_Reflash
{
    //
    // Hex file management
    //
    public class HexFileManager
    {
        // Constants
        public const int KB = 1024;
        public const int MB = (KB * KB);

        // PIC Virtual Memory addresses
        public const uint BOOT_SECTOR_BEGIN = 0x9FC00000;
        public const uint APPLICATION_START = 0x9D000000;

        // Record types
        public const int DATA_RECORD = 0;
        public const int END_OF_FILE_RECORD = 1;
        public const int EXT_SEG_ADRS_RECORD = 2;
        public const int EXT_LIN_ADRS_RECORD = 4;

        // Virtual memory
        private byte[] _virtualFlash = new byte[5 * MB];

        // File Stream and Stream reader
        private FileStream _hexFileStream;
        private StreamReader _hexStreamReader;
        private string _hexFilePath;

        // File details as properties
        public int HexCurrLineNo { get; set; }
        public int HexTotalLines { get; set; }
        public string HexLine { get; set; }

        // Program details as properties
        public uint ProgramStartAddress { get; set; }
        public uint ProgramLength { get; set; }
        public ushort ProgramCrc { get; set; }

        // Constructor
        public HexFileManager()
        {
            // Clear member variables
            _hexFileStream = null;
            _hexStreamReader = null;
            _hexFilePath = null;

            // Clear properties
            ProgramStartAddress = 0;
            ProgramLength = 0;
            ProgramCrc = 0;
            HexCurrLineNo = 0;
            HexTotalLines = 0;
            HexLine = string.Empty;
        }

        // Load a hex file from disk
        public bool LoadHexFile()
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".hex";
            dlg.Filter = "Hex Files (*.hex)|*.hex";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();
            if (!(result == true))
            {
                return false;
            }
            _hexFilePath = dlg.FileName;

            // Validate the path by opening file and counting lines
            try
            {
                HexTotalLines = 0;
                FileStream fs = new FileStream(_hexFilePath, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs);
                while (!sr.EndOfStream)
                {
                    sr.ReadLine();
                    HexTotalLines++;
                }
                sr.Close();
                fs.Close();
            } catch {
                return false;
            }
            // File open successful
            return true;
        }

        // Read a hex record from disk
        public int GetNextHexRecord()
        {
            HexLine = string.Empty;
            if (!_hexStreamReader.EndOfStream) {
                HexLine = ConvertAsciiToHex(_hexStreamReader.ReadLine());
                HexCurrLineNo++;
            }
            return HexLine.Length;
        }

        // Reopen the hex file at the first record
        public bool ResetHexFilePointer()
        {
            if (_hexStreamReader != null)
            {
                _hexStreamReader.Close();
                _hexFileStream.Close();
                _hexStreamReader = null;
                _hexFileStream = null;
            }
            _hexFileStream = new FileStream(_hexFilePath, FileMode.Open, FileAccess.Read);
            _hexStreamReader = new StreamReader(_hexFileStream);
            HexCurrLineNo = 0;
            HexLine = string.Empty;
            return true;
        }

        // Convert ASCII hex to bytes
        public string ConvertAsciiToHex(string ascii_line)
        {
            StringBuilder result = new StringBuilder();

            // Expect the first character to be a ":"
            if (ascii_line[0] != ':') {
                return string.Empty;
            }
            // Expect a even number of hex characters
            if ((ascii_line.Length - 1) % 2 != 0) {
                return string.Empty;
            }
            // Process each pair of hex characters - skip the initial colon
            for (int i = 1; i < ascii_line.Length; i += 2) {
                string bb = ascii_line.Substring(i, 2);
                result.Append((char) Convert.ToByte(bb, 16));
            }
            return result.ToString();
        }

        // Read hex file and calculate CRC
        public void VerifyFlash()
        {
            uint MaxAddress, MinAddress, RecDataLen, RecType, Address;
            uint VirtualFlashAdrs, ExtLinAddress, ExtSegAddress, ProgAddress;
            string RecData;

            // Virtual Flash Erase (Set all bytes to 0xFF)
            for (int i = 0; i < _virtualFlash.Length; i++) {
                _virtualFlash[i] = 0xFF;
            }

            // Reset file pointer and local variables
            ResetHexFilePointer();
	        MaxAddress = 0;
	        MinAddress = 0xFFFFFFFF;
            ExtLinAddress = 0;
            ExtSegAddress = 0;

            // Start decoding the hex file and write into virtual flash
            while (GetNextHexRecord() != 0)
	        {
		        RecDataLen = HexLine[0];
		        RecType = HexLine[3];	
		        RecData = HexLine.Substring(4);
		        switch(RecType)
		        {
                    //Record Type 00, data record
			        case DATA_RECORD:  
                        uint temp_5 = (uint)(HexLine[1] << 8);
                        uint temp_6 = (uint)(HexLine[2]);
				        Address = (((temp_5 & 0x0000FF00U) | (temp_6 & 0x000000FFU)) & (0x0000FFFFU));
				        Address = Address + ExtLinAddress + ExtSegAddress;
		                // Calculate PIC VM address
				        ProgAddress = pa_to_kva0(Address);
                        // Make sure we are not writing boot sector.
				        if(ProgAddress < BOOT_SECTOR_BEGIN)
				        {
					        if(MaxAddress < (ProgAddress + RecDataLen))
					        {
						        MaxAddress = ProgAddress + RecDataLen;
					        }
					        if(MinAddress > ProgAddress)
					        {
						        MinAddress = ProgAddress;
					        }
                            // Calculate PC memory array virtual flash address
					        VirtualFlashAdrs = pa_to_vfa(ProgAddress);
                            for (int i = 0; i < RecDataLen; i++)
                            {
                                _virtualFlash[VirtualFlashAdrs + i] = (byte) RecData[i];
                            }
				        }
				        break;
                    // Record Type 02 - segment address
			        case EXT_SEG_ADRS_RECORD:
                        // Record Type 02, defines 4 to 19 of the data address.
                        uint temp_1 = (uint) (RecData[0] << 12);
                        uint temp_2 = (uint) (RecData[1] << 4);
				        ExtSegAddress = ((temp_1 & 0x000FF000U) | (temp_2 & 0x00000FF0U));				
				        ExtLinAddress = 0;
				        break;
			        // Record type 04 - extended address
			        case EXT_LIN_ADRS_RECORD:
                        uint temp_3 = (uint) (RecData[0] << 24);
                        uint temp_4 = (uint) (RecData[1] << 16);
				        ExtLinAddress = ((temp_3 & 0xFF000000U) | (temp_4 & 0x00FF0000U));
				        ExtSegAddress = 0;
				        break;
                    //Record Type 01
			        case END_OF_FILE_RECORD:  
			        default: 
				        ExtSegAddress = 0;
				        ExtLinAddress = 0;
				        break;
		        }	
	        }
            // Round address to word boundaries
	        MinAddress -= MinAddress % 4;
	        MaxAddress += MaxAddress % 4;
            // Calculate start and length
	        ProgramLength = MaxAddress - MinAddress;
	        ProgramStartAddress = MinAddress;
	        VirtualFlashAdrs = pa_to_vfa(MinAddress);
	        ProgramCrc = Utility.CalculateCrc(_virtualFlash, VirtualFlashAdrs, ProgramLength);	
        }

        // Convert a physical memory address to a PC memory array virtual address
        private uint pa_to_vfa(uint address)
        {
            return (address - APPLICATION_START);
        }

        // Convert a physical memory address to a PIC memory manager virtual address
        private uint pa_to_kva0(uint address)
        {
            return (address | 0x80000000);
        }
    }
}

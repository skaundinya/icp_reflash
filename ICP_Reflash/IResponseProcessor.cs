﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICP_Reflash
{
    public interface IResponseProcessor
    {
        void process_response(BootLoaderCommand cmd, byte[] response);
        void process_timeout(BootLoaderCommand cmd);
    }
}
